#! /bin/bash
TMP="gandm"
PAGE_SESSION="56266FD04F744A6BPQ"

BASEPATH=$(pwd)
# ~~ main files
COOKIE="$BASEPATH/cookies.txt"
PREFIX="http://search.proquest.com.res.banq.qc.ca"
USER_AGENT="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"

# ~~ directories and files
TMP_DIR="$BASEPATH/junk"
PDF_DIR="$BASEPATH/pdf"
DONE_DIR="$BASEPATH/done"

WORK_FILE="wget.tmp"

function clean(){
	echo "cleaning up..."
	rm -f done/*
	rm -f junk/*
	rm -f pdf/*
}

function wget_file(){
	#takes url, directory
	echo "wgetting file and saving as $2"
	wget --random-wait --wait=20 --user-agent="$USER_AGENT" --keep-session-cookies --save-cookies="$COOKIE" --load-cookies="$COOKIE" "$1" -O "$2" 2> /dev/null
}

function get_pdf(){
	echo "getting pdf for $1"
	PAGE="$PREFIX$1"

	NAME=$(echo $1 | cut -d "/" -f 4)
	HTML_FILE="$DONE_DIR/details.$NAME.html"

   	wget_file "$PAGE" "$HTML_FILE"
   	BREAK=$(grep -e "Session terminée" -e "Shibboleth Authentication Request" -e "403 Forbidden" $HTML_FILE)
	if [[ $BREAK != "" ]]
	then
		echo "Session terminated. Please restart."
		exit
	fi
   	# do something with this --

   	# PDF_TITLE=($(pup -p -f $HTML_FILE 'div.headerForDocviewPage h2 text{}'))
   	# PDF_TITLE=($(pup -p -f $HTML_FILE 'a#lateralSearch text{}'))
   	# PDF_TITLE=($(pup -p -f $HTML_FILE 'div.headerForDocviewPage h2 text{}'))
   	# PDF_TITLE=($(pup -p -f $HTML_FILE 'div.headerForDocviewPage h2 text{}'))
	PDF_URL=$(pup -p -f $HTML_FILE '#pdfdiv a attr{href}')

	PDF_FILE="$PDF_DIR/$NAME.pdf"
	wget_file "${PDF_URL}" "$PDF_FILE"
	# check to make sure the result file is a pdf?
}

function get_index_pages(){
	# get the index pages from PF to PT
	PF=1
	PF=10

	for NUMBER in $(seq $PF $PT)
	do
		echo "downloading page $NUMBER"
		PAGE="$PREFIX/results.bottompagelinks:gotopage/$NUMBER?site=hnpglobeandmail&t:ac=$PAGE_SESSION/1#scrollTo"
		INDEX_FILE="$DONE_DIR/$TMP.$NUMBER.html"
		wget_file "$PAGE" "$INDEX_FILE"
		
		BREAK=$(grep -e "Session terminée" -e "Shibboleth Authentication Request" $INDEX_FILE)
		if [[ $BREAK != "" ]]
		then
			echo "Session terminated. Please restart."
			exit
		fi

		ARTICLES=($(pup -f "$INDEX_FILE" 'a.Topicsresult attr{href}'))
		# there are as many articles on the page as article elements, so use that in sequencing
		REC_NUM=${#ARTICLES[@]}

		for ARTICLE in $(seq 0 "$REC_NUM")
		do
			get_pdf ${ARTICLES[$ARTICLE]}
			# get_pdf $ARTICLES[$SEQ_NUM]
			sleep 15
		done
	done
	cd ..
}

clean
get_index_pages


